# Go, Messenger!
## Game Development - Game Jam Project
----
### **Alur Cerita**

*Disclaimer*: Alur cerita pada permainan ini merupakan cerita fiksi yang menggunakan latar peristiwa non-fiksi

Pada saat perang dunia 2, pemerintah negara Inggris mengirimkan seorang pria yang sudah handal untuk menyelidiki sesuatu untuk mencari informasi sehingga pria tersebut dijuluki "Messenger". Kali ini, pria tersebut ditugaskan untuk menyelidiki aktivitas tentara negara Jerman dengan cara menyusup ke dalamnya untuk bisa mengetahui kapan Jerman akan menyerang daerah teritorial negara Inggris. Pada awalnya, penyelidikan berjalan sangat lancar hingga suatu ketika, pria tersebut membaca salah satu dokumen mengenai rencana penyerangan negara Inggris yang dijadwalkan tidak lama setelah pria tersebut membaca dokumen tersebut. Karena itu, pria tersebut harus kembali ke negara asalnya dengan cepat agar dia bisa memberitahu pemerintah Inggris bahwa negara tersebut akan diserang sehingga pemerintahan Inggris bisa mempersiapkan diri untuk bertahan dari serangan. Akan tetapi, saat pria tersebut dalam perjalanan, dia harus melewati sebuah lapangan luas di dalam teritorial negara Jerman yang berisikan banyak ranjau yang tertanam di dalam tanahnya. Apabila ranjau tersebut diinjak, maka ranjau tersebut akan meledak dan tentu akan membuat apapun yang menginjaknya menjadi hancur. Selain itu, terdapat 2 jalan yang bisa digunakan untuk bisa melewati lapangan tersebut yang telah diketahui bahwa hanya 1 jalan keluar yang akan dapat membawa pria tersebut keluar dari lapangan tersebut dan jalan lainya akan membuat pria tersebut tertangkap oleh tentara Jerman. Dengan berbekal radar ranjau yang berhasil pria itu dapatkan, pria tersebut harus dengan cepat melewati lapangan tersebut agar bisa menyelamatkan negaranya. 

### **Cara Bermain**

Pada permainan ini, pemain akan berperan sebagai "Messenger" yang memiliki tujuan untuk melewati lapangan luas yang penuh dengan ranjau. Pemain hanya dapat bergerak menggunakan tombol panah yang ada pada *keyboard* sehingga pemain hanya bisa bergerak dalam 4 arah, yaitu atas, bawah, kanan dan kiri sesuai dengan petak yang telah ditentukan. Ketika ada dalam permainan, berikut adalah tampilan yang dilihat oleh pemain:

![Tampilan In-game](Images/gameplay1.PNG)

Petak yang berwarna hijau menandakan petak yang dapat dijadikan tempat pemain untuk bergerak sedangkan petak yang berwarna abu abu merupakan petak yang menandakan "tepi" dari area permainan. Tampilan yang ada di bagian bawah menunjukan hal-hal yang diberitahu oleh radar ranjau yang dimiliki oleh pemain. Dari tampilan tersebut dapat diketahui, dari petak di sebelah kanan pemain maupun tetangga dari petak tersebut terdapat 2 petak yang berisi ranjau. Yang dimaksud tetangga dari suatu petak adalah sebagai berikut:

![Tampilan In-game](Images/gameplay2.PNG)

Hal tersebut juga berlaku untuk semua arah lainya (atas, bawah, dan kiri). Apabila petak yang ada di salah satu sisi pemain merupakan tepi, maka akan ditampilkan "--" pada radar ranjau. Dan untuk menghitung banyaknya ranjau, petak yang merupakan sisi tidak akan diikutsertakan dalam perhitungan.

Pemain diberikan 10 kali kesempatan dalam satu kali permainan untuk mencari jalan yang akan membuat pemain bisa mengirimkan informasi yang dimilikinya ke negara asalnya. Selain itu, pemain juga memiliki batasan untuk tidak bergerak terlalu banyak karena apabila pemain bergerak terlalu banyak, itu dapat menyebabkan pemain menjadi terlambat dalam mengirimkan informasi ke negara asalnya yang akan mengakibatkan negara tersebut diserang terlebih dahulu sebelum bisa mempersiapkan diri untuk bertahan dari serangan.

### **Asset**

Dalam membuat permainan ini, pihak pengembang menggunakan *asset* yang tersedia secara gratis. Berikut adalah detail melalui *website* apa pihak pengembang mendapatkan *asset* untuk membuat permainan ini:

1. Karakter: https://craftpix.net/freebies/free-3-character-sprite-sheets-pixel-art/
2. Latar permainan: https://craftpix.net/freebies/free-battle-location-top-down-game-tileset-pack-1/
3. *Font*: https://www.1001fonts.com/call-of-ops-duty-font.html 
4. Audio: 
   - https://kenney.nl/assets?q=audio 
   - https://www.freesoundeffects.com/free-sounds/
   - https://soundcloud.com/ashamaluevmusic/sets/war-music


