extends Node2D

var map = [[0,0,1,0,0,1,0,1,0,1,0,0,0,0],[1,0,0,0,1,0,0,0,0,0,0,0,1,0],[0,0,1,0,1,0,0,0,1,0,1,0,0,1],[1,0,0,1,0,0,0,0,0,0,0,0,0,0],[0,0,0,1,0,0,0,0,0,0,1,0,0,0],[1,0,0,0,0,1,0,1,0,1,0,1,0,0],[0,0,0,0,0,1,0,0,0,0,0,1,0,0]]
var max_move = 35
var pos = null
var min_y = 0
var min_x = 0
var max_y = 6
var max_x = 13

var player_x = null
var player_y = null



func _process(delta):
	pos = $Player.player_pos
	player_x = 96+(64*pos[1])
	player_y = 96+(64*pos[0])
	if Input.is_action_just_released("mark_up"):
		if pos[0] > min_y:
			put_flag("up")
	elif Input.is_action_just_released("mark_down"):
		if pos[0] < max_y:
			put_flag("down")
	elif Input.is_action_just_released("mark_left"):
		if pos[1] > min_x:
			put_flag("left")
	elif Input.is_action_just_released("mark_right"):
		if pos[1] < max_x:
			put_flag("right")
	elif Input.is_action_just_released("ui_up"):
		if pos[0] > min_y:
			put_stone("up")
	elif Input.is_action_just_released("ui_down"):
		if pos[0] < max_y:
			put_stone("down")
	elif Input.is_action_just_released("ui_right"):
		if pos[1] < max_x:
			put_stone("right")
	elif Input.is_action_just_released("ui_left"):
		if pos[1] > min_x:
			put_stone("left")

func put_flag(direction):
	$Put.play()
	var tmp = preload("res://Scene/BombFlag.tscn")
	var flag = tmp.instance()
	add_child(flag)
	if direction == "up":
		flag.position.x = player_x
		flag.position.y = player_y - 64
	elif direction == "down":
		flag.position.x = player_x
		flag.position.y = player_y + 64
	elif direction == "right":
		flag.position.x = player_x + 64
		flag.position.y = player_y
	elif direction == "left":
		flag.position.x = player_x - 64
		flag.position.y = player_y

func put_stone(direction):
	var tmp = preload("res://Scene/visitFlag.tscn")
	var flag = tmp.instance()
	add_child(flag)
	flag.position.x = player_x
	flag.position.y = player_y 
	
