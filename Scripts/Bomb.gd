extends Area2D

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		Global.lives = Global.lives - 1
		if Global.lives > 0:
			get_tree().change_scene(str("res://Scene/RemainingLive.tscn"))
		else:
			get_tree().change_scene(str("res://Scene/Lose.tscn"))
