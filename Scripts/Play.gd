extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	Global.lives = 10
	get_tree().change_scene(str("res://Scene/" + scene_to_load + ".tscn"))
