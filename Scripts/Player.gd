extends KinematicBody2D

var tile_size = 64
var inputs = {"right": Vector2.RIGHT,
			"left": Vector2.LEFT,
			"up": Vector2.UP,
			"down": Vector2.DOWN}
var movement = 0

var min_y = 0
var min_x = 0
var max_y = 6
var max_x = 13

var map = null

var player_pos = [0,0]

var bomb_count = [-1,-1,-1,-1] #[up,right,left,down]
var step = 0

func _ready():
	map = get_node("..").map
	check_bomb()
	position = position.snapped(Vector2.ONE * tile_size)
	position += Vector2.ONE * tile_size/2

func _physics_process(delta):
	var is_moving = false
	if Input.is_action_just_released("ui_up"):
		if player_pos[0] > min_y:
			player_pos[0] = player_pos[0] - 1
			move("up")
			is_moving = true
		elif player_pos[0] == 0 and player_pos[1] == 11:
			var max_step = get_node("..").max_move
			if step <= max_step:
				get_tree().change_scene(str("res://Scene/GoodEnding.tscn"))
			else:
				get_tree().change_scene(str("res://Scene/BadEnding2.tscn"))
	elif Input.is_action_just_released("ui_down"):
		if player_pos[0] < max_y:
			player_pos[0] = player_pos[0] + 1
			move("down")
			is_moving = true
	elif Input.is_action_just_released("ui_left"):
		if player_pos[1] > min_x:
			player_pos[1] = player_pos[1] - 1
			move("left")
			is_moving = true
	elif Input.is_action_just_released("ui_right"):
		if player_pos[1] < max_x:
			player_pos[1] = player_pos[1] + 1
			move("right")
			is_moving = true
		elif player_pos[1] == max_x and player_pos[0] == max_y:
			get_tree().change_scene(str("res://Scene/BadEnding.tscn"))
	if is_moving:
		check_bomb()

func move(dir):
	position += inputs[dir] * (tile_size)
	get_node("Move").play()
	step += 1

func check_bomb():
	var poss_move = [[-1,0],[0,1],[0,-1],[1,0]]
	for i in range(4):
		var can_move = check_pos(poss_move[i])
		if can_move:
			bomb_count[i] = check(poss_move[i])
		else:
			bomb_count[i] = -1
	for i in range(4):
		if i == 0:
			if bomb_count[i] == -1:
				get_node("../Up").text = "--"
			else:
				get_node("../Up").text = str(bomb_count[i])
		elif i == 1:
			if bomb_count[i] == -1:
				get_node("../Right").text = "--"
			else:
				get_node("../Right").text = str(bomb_count[i])
		elif i == 2:
			if bomb_count[i] == -1:
				get_node("../Left").text = "--"
			else:
				get_node("../Left").text = str(bomb_count[i])
		else:
			if bomb_count[i] == -1:
				get_node("../Down").text = "--"
			else:
				get_node("../Down").text = str(bomb_count[i])
	
func check_pos(move):
	if move[0] == 0:
		return (player_pos[1] + move[1] >= min_x) and  (player_pos[1] + move[1] <= max_x) 
	else:
		return (player_pos[0] + move[0] >= min_y) and  (player_pos[0] + move[0] <= max_y)  

func check(move):
	var new_pos = [player_pos[0]+move[0],player_pos[1]+move[1]]
	var poss_grid = [[-1,0],[0,1],[0,-1],[1,0],[0,0],[-1,-1],[-1,1],[1,-1],[1,1]]
	var bomb = 0
	for i in range(9):
		var check_position = [new_pos[0]+poss_grid[i][0],new_pos[1]+poss_grid[i][1]]
		var can_check = edge_check(check_position)
		if can_check:
			if map[check_position[0]][check_position[1]] == 1:
				bomb += 1
	return bomb
	
func edge_check(coor):
	return (coor[0] >= min_y and coor[0] <= max_y) and (coor[1] >= min_x and coor[1] <= max_x)
	
	
